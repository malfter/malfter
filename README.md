### Hi, I'm Markus 👋

I use GitLab to manage some private projects. In my free time I try to support projects from [kamapu.net](https://kamapu.net).

Most of the projects I host here on [GitLab.com](https://gitlab.com) or in a private [Forgejo](https://forgejo.org) instance. Some projects can also be found on [GitHub](https://github.com/malfter).

You can find my website (or better said my link collection 😊) [here](https://alfter-web.de/). If you want to contact me you can find me on <a rel="me" href="https://chaos.social/@markusalfter">Mastodon</a>.

<!-- markdownlint-disable MD033 MD041 -->
<a href="https://www.deine-berge.de/POI/Pass-Uebergang/Oesterreich/Stubaier-Alpen/Hoehe-2881m/5180/Grabagrubennieder.html"><img src="https://gitlab.com/malfter/malfter/-/raw/main/profile.png"  title="Grabagrubennieder 2881m"/></a>
